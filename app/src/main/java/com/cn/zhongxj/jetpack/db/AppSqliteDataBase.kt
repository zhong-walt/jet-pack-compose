package com.cn.zhongxj.jetpack.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.cn.zhongxj.jetpack.db.dao.SearchHistoryDao
import com.cn.zhongxj.jetpack.db.dao.TestDao
import com.cn.zhongxj.jetpack.db.entity.SearchHistory
import com.cn.zhongxj.jetpack.db.entity.TestEntity

@Database(
    version = 2,//数据库的版本
    entities = [
        SearchHistory::class,
        TestEntity::class
    ] // 和表相互映射的实体类
)
abstract class AppSqliteDataBase:RoomDatabase(){
    abstract fun searchHistoryDao():SearchHistoryDao
    abstract fun testEntityDao():TestDao
}

class AppDB{
    companion object{
        fun init(context: Context):AppSqliteDataBase{
            val databaseBuilder = Room.databaseBuilder(
                context = context,
                klass = AppSqliteDataBase::class.java,
                name = "SearchDB"
            ).apply {
                fallbackToDestructiveMigration()
                addMigrations( // 数据库升级迁移
                    MIGRATION_1_2
                )
            }

            return databaseBuilder.build()
        }
    }
}