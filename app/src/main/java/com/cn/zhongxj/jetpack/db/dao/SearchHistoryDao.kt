package com.cn.zhongxj.jetpack.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.cn.zhongxj.jetpack.db.entity.SearchHistory

@Dao
interface SearchHistoryDao {
    @Query("select * from t_search_history where isDelete = 0 order by create_time desc limit 10")
    fun getAllHistory():MutableList<SearchHistory>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHistory(history: SearchHistory):Long

    @Query("delete from t_search_history")
    suspend fun clearAll()

    @Transaction
    @Query("select * from t_search_history where id=:id")
    suspend fun findSearchHistoryById(id:Long):SearchHistory?

    @Update
    suspend fun update(searchHistory: SearchHistory)
}