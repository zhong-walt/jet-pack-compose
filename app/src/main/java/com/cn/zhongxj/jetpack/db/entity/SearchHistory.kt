package com.cn.zhongxj.jetpack.db.entity

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "t_search_history")
data class SearchHistory(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0L,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "webUrl")
    val webUrl: String,
    @ColumnInfo(name = "create_time")
    val createTime: Long = 0L,
    // 是否已经删除，0表示未删除，1表示已删除
    @ColumnInfo(name = "isDelete")
    var isDelete: String = "0",
)
