package com.cn.zhongxj.jetpack.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cn.zhongxj.jetpack.db.entity.TestEntity

@Dao
interface TestDao {
    @Query("select * from t_test")
    fun getAll():MutableList<TestEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTest(testEntity: TestEntity):Long
}