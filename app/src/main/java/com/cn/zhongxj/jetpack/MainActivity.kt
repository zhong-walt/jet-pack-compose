package com.cn.zhongxj.jetpack

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cn.zhongxj.jetpack.ui.SearchPage
import com.cn.zhongxj.jetpack.ui.model.SearchHistoryState
import com.cn.zhongxj.jetpack.ui.model.SearchHistoryViewModel

@Suppress("UNCHECKED_CAST")
class MainActivity : ComponentActivity() {
    private val TAG = "MainActivity===>"

    private val searchHistoryViewModel by viewModels<SearchHistoryViewModel>(factoryProducer = {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return SearchHistoryViewModel(
                    MyApp.SHDB
                ) as T
            }
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        searchHistoryViewModel.loadHistoryList() // 加载搜索记录的列表
        setContent {
            DisposableEffect(Unit) {
                enableEdgeToEdge(
                    statusBarStyle = SystemBarStyle.auto(
                        android.graphics.Color.TRANSPARENT,
                        android.graphics.Color.TRANSPARENT
                    ) {
                        false
                    },
                    navigationBarStyle = SystemBarStyle.auto(
                        lightScrim,
                        darkScrim
                    ) {
                        false
                    }
                )
                onDispose { }
            }
            // A surface container using the 'background' color from the theme
            Surface(
                modifier = Modifier.fillMaxSize()
                    .background(Color.White)
                    .statusBarsPadding(),
                color = MaterialTheme.colorScheme.background
            ) {
                SearchPage(searchHistoryViewModel.searchHistoryStateList,
                    onBackClick = { finish() },
                    onSearchTrigger = { url ->
                        if (url.isNotEmpty()) {
                            val searchHistoryState = SearchHistoryState(
                                history_title = url,
                                history_content = url,
                                createTime = System.currentTimeMillis()
                            )

                            searchHistoryViewModel.addHistory(searchHistoryState)
                        }
                    },
                    onClearAllClick = {
                        searchHistoryViewModel.clearAllHistory()
                    },
                    onDeleteClick = { searchHistoryState ->
                        Log.d(TAG, "onDeleteClick=>searchHistoryState: $searchHistoryState")
                        searchHistoryViewModel.deleteHistory(searchHistoryState)

                    },
                    onHistoryItemClick = { searchHistoryState ->
                        Log.d(TAG, "onHistoryItemClick=>searchHistoryState: $searchHistoryState")
                        val content = searchHistoryState.history_content
                        val searchHistory = SearchHistoryState(
                            history_title = content,
                            history_content = content,
                            createTime = System.currentTimeMillis()
                        )

                        searchHistoryViewModel.updateHistory(searchHistory)
                    }
                )
            }
        }
    }

    private val lightScrim = android.graphics.Color.argb(0xe6, 0xFF, 0xFF, 0xFF)
    private val darkScrim = android.graphics.Color.argb(0x80, 0x1b, 0x1b, 0x1b)

    inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T = f() as T
        }
}
