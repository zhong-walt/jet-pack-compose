package com.cn.zhongxj.jetpack.ui.model

import android.util.Log
import androidx.compose.runtime.Stable
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cn.zhongxj.jetpack.db.AppSqliteDataBase
import com.cn.zhongxj.jetpack.db.entity.SearchHistory
import com.cn.zhongxj.jetpack.db.entity.TestEntity
import com.cn.zhongxj.jetpack.db.repository.SearchHistoryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchHistoryViewModel(db: AppSqliteDataBase) : ViewModel() {
    private val TAG = "SearchHistoryViewModel"
    var searchHistoryRepo: SearchHistoryRepository = SearchHistoryRepository(db = db)
    var searchHistoryStateList = mutableStateListOf<SearchHistoryState>()

    fun loadHistoryList() {
        Log.d(TAG, "loadHistoryList")

       viewModelScope.launch(Dispatchers.IO) {
           searchHistoryRepo.getSearchHistoryList().forEach { searchHistory: SearchHistory ->
               Log.d(TAG,"loadHistoryList: $searchHistory")
               val searchHistoryState = SearchHistoryState(
                   history_id = searchHistory.id,
                   history_title = searchHistory.title,
                   history_content = searchHistory.webUrl,
                   createTime = searchHistory.createTime,
                   isDelete = searchHistory.isDelete
               )

               searchHistoryStateList.add(searchHistoryState)
           }

           searchHistoryRepo.insertTest(TestEntity(name = "walt"))
           searchHistoryRepo.insertTest(TestEntity(name = "zhong"))
           searchHistoryRepo.insertTest(TestEntity(name = "007"))


           searchHistoryRepo.getTestList().forEach {
               Log.d(TAG,"result: $it")
           }
       }
    }

    fun deleteHistory(searchHistoryState: SearchHistoryState) {
        Log.d(TAG, "deleteHistory: $searchHistoryState")
        viewModelScope.launch(Dispatchers.IO) {
            searchHistoryStateList.remove(searchHistoryState)
            searchHistoryStateList.sortBy { it.createTime }
            searchHistoryRepo.deleteById(searchHistoryState.history_id)
        }
    }

    fun addHistory(searchHistoryState: SearchHistoryState) {
        Log.d(TAG, "deleteHistory: $searchHistoryState")
        if(searchHistoryStateList.size == 10){
            searchHistoryStateList.removeLast()
        }

        viewModelScope.launch(Dispatchers.IO) {
            searchHistoryStateList.add(searchHistoryState)
            searchHistoryStateList.sortBy { it.createTime }
            val searchHistory = SearchHistory(
                title = searchHistoryState.history_title,
                webUrl = searchHistoryState.history_content,
                createTime = searchHistoryState.createTime
            )
            searchHistoryRepo.insertHistory(searchHistory)
        }
    }

    fun clearAllHistory() {
        Log.d(TAG, "clearAllHistory")
        searchHistoryStateList.clear()
        viewModelScope.launch {
            searchHistoryRepo.clearAllHistory()
        }
    }

    fun updateHistory(searchHistoryState: SearchHistoryState){
        viewModelScope.launch {
            val searchHistory = SearchHistory(
                title = searchHistoryState.history_title,
                webUrl = searchHistoryState.history_content,
                createTime = searchHistoryState.createTime
            )
            searchHistoryRepo.updateHistory(searchHistory)
        }
    }
}

@Stable
data class SearchHistoryState(
    var history_id: Long = -1L,
    var history_title: String = "",
    var history_content:  String = "",
    var createTime: Long = System.currentTimeMillis(),
    var isDelete:  String = "0"
)