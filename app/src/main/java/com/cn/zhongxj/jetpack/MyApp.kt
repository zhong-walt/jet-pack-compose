package com.cn.zhongxj.jetpack

import android.app.Application
import com.cn.zhongxj.jetpack.db.AppDB
import com.cn.zhongxj.jetpack.db.AppSqliteDataBase

class MyApp : Application() {
    companion object{
        lateinit var SHDB:AppSqliteDataBase
    }
    override fun onCreate() {
        super.onCreate()
        SHDB = AppDB.init(context = this)
    }
}